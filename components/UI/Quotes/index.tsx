import React, {FC, useEffect, useState} from 'react';
import {Container, QuotesContainer, Title} from "./styles";
import Card from "../../Card";
import {QuoteResponse, QuotesProps} from "../../../types/globalTypes";
import {QuoteState} from "../../Card/types";
import {useLocalStorage} from "../../../utils/useLocalStorage";


const Quotes: FC<QuotesProps> = ({quotes}) => {

  const equals = (arr1: Array<any>, arr2:Array<any>) => {
    return (
      arr1.length === arr2.length &&
      arr1.every((value, index) => value === arr2[index])
    );
  }

  const [localQuotes, setQuotes] = useLocalStorage<QuoteResponse[]>('quotes', [])
  const [quotesState, setQuotesState] = useState<QuoteResponse[]>([])

  useEffect(() => {
    setQuotesState(equals(localQuotes, []) ? quotes : localQuotes)
  }, [])

  const like = (quote: string, author: string, liked: boolean) => {
    quotesState.map(item => {
      if (item.q !== quote) {
        const newQuote: QuoteResponse = {
          q: quote,
          liked,
          a: author,
          c: "",
          h: ""
        }
        setQuotesState(prevState => [...prevState, newQuote])
      }
    })

    const changedQuote = quotesState.map(item => {
      if (item.q === quote) {
        item.liked = !item.liked
      }

      return item
    })
    setQuotesState(changedQuote)
    localStorage.setItem('quotes', JSON.stringify(quotesState))
    setQuotes(quotesState)
  }


  return (
    <Container>
      <QuotesContainer>
        <Title>Popular quotes</Title>
        {
          quotesState && quotesState.map((item, index) =>
            <Card
              author={item.a}
              quote={item.q}
              like={like}
              liked={item.liked}
              key={index}
            />
          )}
      </QuotesContainer>
    </Container>
  );
};

export default Quotes;
