import styled from "styled-components";

export const Container = styled.div`
  height: fit-content;
  display: flex;
  justify-content: center;
`

export const QuotesContainer = styled.div`
  padding: 30px;
  min-width: 60vw;
  background-color: #fff;
`

export const Title = styled.div`
  margin-bottom: 30px;
  font-weight: 600;
  font-size: 24px;
  color: #000;
  text-align: center;
`