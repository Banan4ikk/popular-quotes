import styled from "styled-components";

export const MenuContainer = styled.div`
  display: flex;
  flex-direction: row;
  transform: translateY(5px);
`

export const MenuItem = styled.a`
  margin-left: 30px;
  height: fit-content;
  text-decoration: none;
  color: #fff;
  font-weight: 400;

  &:hover {
    cursor: pointer;
  }
  &:after {
    content: '';
    margin-top: 8px;
    position: relative;
    width: 0;
    display: flex;
    height: 2px;
    background: #fff;
    transition: width 0.2s ease-in-out;
    color: #fff;
  }
  &:hover::after {
    width: 100%;
  }
  &:link {
    text-decoration: none;
    color: #fff;
  }
`;
