import React, {FC} from 'react';
import {MenuContainer, MenuItem} from './styles';

const Menu:FC = () => {
  return (
    <MenuContainer>
    <MenuItem>Home</MenuItem>
    <MenuItem>My Books</MenuItem>
    <MenuItem>Browse</MenuItem>
    <MenuItem>Community</MenuItem>
    </MenuContainer>
  );
};

export default Menu;