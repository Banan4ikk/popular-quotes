import React from 'react';
import {Wrapper} from "./styles";
import {NextPage} from "next";
import {ChildrenProp} from "../../../types/globalTypes";

const AppWrapper:NextPage<ChildrenProp> = ({children}) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
};

export default AppWrapper;