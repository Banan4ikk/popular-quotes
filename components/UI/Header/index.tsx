import React, {FC} from 'react';
import {HeaderContainer, StyledLogo} from "./styles";
import {ChildrenProp} from "../../../types/globalTypes";

const Header: FC<ChildrenProp> = ({children}) => {
  return (
    <HeaderContainer>
      <StyledLogo src='logo.png' />
      {children}
    </HeaderContainer>
  );
};

export default Header;