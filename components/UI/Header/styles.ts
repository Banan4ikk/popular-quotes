import styled from "styled-components";

export const HeaderContainer = styled.div`
  min-height: 90px;
  width: 100vw;
  background-color: #312539;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  padding: 0 350px;
  align-items: center;
`;

export const StyledLogo = styled.img`
  max-height: 90px;
  transform: translateX(-50px);
`