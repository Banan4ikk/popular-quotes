import styled from "styled-components";

export const FooterContainer = styled.div`
  min-height: 300px;
  width: 100vw;
  background-color: #312539;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
`;