import React, {FC} from 'react';
import {FooterContainer} from "./styles";

const Footer: FC = () => {
  return (
    <FooterContainer>Some Info</FooterContainer>
  );
};

export default Footer;