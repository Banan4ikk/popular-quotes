import React, {FC, useEffect, useState} from 'react';
import {CardContainer, QuoteTextContainer, StyledLike, StyledPhoto, Text, TextContainer} from "./styles";
import {QuoteProps, QuoteState} from "./types";
import {QuotesProps} from "../../types/globalTypes";

const Card: FC<QuoteProps> = ({quote, author, like, liked}) => {
  const [isLiked, setIsLiked] = useState(liked ? liked : false)

  const setLike = () => {
    setIsLiked(!isLiked)
    like(quote, author, isLiked)
  }

  return (
    <>
      <CardContainer>
        <StyledPhoto src='https://images.gr-assets.com/authors/1521044377p2/3565.jpg'/>
        <TextContainer>
          <QuoteTextContainer>
            <Text>{quote}</Text>
            <StyledLike
              src={isLiked ? 'filled-like.svg' : 'like.svg'}
              onClick={setLike}
            />
          </QuoteTextContainer>
          <Text>― {author}</Text>
        </TextContainer>
      </CardContainer>
    </>
  );
};


export default Card;
