import {QuoteResponse} from "../../types/globalTypes";

export type Props = {
  quotes: QuoteResponse[]
}
export type QuoteProps = {
  quote: string,
  author: string,
  liked: boolean
  like: (quote: string, author: string, liked: boolean) => void
}

export type QuoteState = {
  quote: string,
  liked: boolean,
  author: string
}
