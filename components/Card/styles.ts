import styled from "styled-components";

export const CardContainer = styled.div`
  min-width: 100%;
  padding: 15px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
  background-color: #f5f5f5;
  border-radius: 7px;
  margin-bottom: 20px;

  &:after {
    content: '';
    background-color: #D7D7D7;
    height: 2px;
  }
  &:last-child{
    margin-bottom: 0;
  }
`

export const StyledPhoto = styled.img`
  margin-right: 15px;
`

export const StyledLike = styled.img`
  &:hover{
    cursor: pointer;
  }
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const Text = styled.span`
  color: #000;
  font-size: 18px;
  font-weight: 300;
  margin-bottom: 7px;
  
  &:last-child{
    margin-bottom: 0;
  }
`

export const QuoteTextContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 50vw;
`