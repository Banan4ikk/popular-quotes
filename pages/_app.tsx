import type {AppProps} from 'next/app'
import './../globalStyles.css'

function MyApp({Component, pageProps}: AppProps) {
  return (
    <>
      <link href='https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;600&display=swap' rel='stylesheet'/>
      <Component {...pageProps} />
    </>)

}

export default MyApp
