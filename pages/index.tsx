import type {NextPage} from 'next'
import Header from "../components/UI/Header";
import Menu from '../components/UI/Menu';
import AppWrapper from "../components/UI/AppWrapper";
import Quotes from "../components/UI/Quotes";
import {GetServerSideProps} from "next";
import axios, {AxiosResponse} from "axios";
import {QuoteResponse, QuotesProps} from "../types/globalTypes";
import Footer from "../components/UI/Footer";

const Home: NextPage<QuotesProps> = ({quotes}) => {
  return (
    <AppWrapper>
      <Header>
        <Menu/>
      </Header>
      <Quotes quotes={quotes}/>
      <Footer/>
    </AppWrapper>
  )
}

export const getServerSideProps: GetServerSideProps<QuotesProps> = async (context) => {
  const result: AxiosResponse = await axios.get('https://zenquotes.io/api/quotes')
  const data: QuoteResponse[] = result.data
  return {
    props: {
      quotes: data
    },
  }
}

export default Home
